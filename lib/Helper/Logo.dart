import 'package:flutter/material.dart';

class AppLogo extends StatelessWidget {
  final double logoWidth;

  const AppLogo({
    Key key,
    this.logoWidth,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: logoWidth,
      margin: const EdgeInsets.only(left: 50, right: 50),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          // Image.asset('assets/images/Logo-Vertical.png',
          //     width: double.infinity),
        ],
      ),
    );
  }
}

class LandscapeLogo extends StatelessWidget {
  final double logoHeight;
  final double logoWidth;

  const LandscapeLogo({Key key, this.logoHeight, this.logoWidth}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 50, right: 50),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            'assets/images/Logo-Vertical.png',
            width: logoWidth,
            height: logoHeight,
          ),
        ],
      ),
    );
  }
}
