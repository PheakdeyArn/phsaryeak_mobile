import 'dart:async';
import 'dart:math';

import 'package:eshop/Add_Address.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'Helper/Color.dart';
import 'Helper/Session.dart';
import 'Profile.dart';

class FullMap extends StatefulWidget {
  final double latitude, longitude;
  final String from;
  final Function setAddress;

  const FullMap(
      {Key key, this.latitude, this.longitude, this.from, this.setAddress})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _FullMapState();
}

class _FullMapState extends State<FullMap> {
  LatLng latlong;
  CameraPosition _cameraPosition;
  GoogleMapController _controller;
  TextEditingController locationController = TextEditingController();
  Set<Marker> _markers = Set();

  Function callback;

  Future getCurrentLocation() async {
    List<Placemark> placemark =
        await placemarkFromCoordinates(widget.latitude, widget.longitude);

    if (mounted)
      setState(() {
        latlong = new LatLng(widget.latitude, widget.longitude);

        _cameraPosition =
            CameraPosition(target: latlong, zoom: 15.0, bearing: 0);
        if (_controller != null)
          _controller
              .animateCamera(CameraUpdate.newCameraPosition(_cameraPosition));

        var address;
        address = placemark[0].name;
        address = address + ", " + placemark[0].street;
        address = address + ", " + placemark[0].subLocality;
        address = address + ", " + placemark[0].locality;
        address = address + ", " + placemark[0].administrativeArea;
        address = address + ", " + placemark[0].country;

        locationController.text = address;
        _markers.add(Marker(
          markerId: MarkerId("Marker"),
          position: LatLng(widget.latitude, widget.longitude),
        ));
      });
  }

  @override
  void initState() {
    super.initState();

    callback = widget.setAddress;

    _cameraPosition = CameraPosition(target: LatLng(0, 0), zoom: 10.0);
    getCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: getAppBar(getTranslated(context, 'CHOOSE_LOCATION'), context),
        body: SafeArea(
            child: Column(
          children: <Widget>[
            Expanded(
              child: Stack(children: [
                (latlong != null)
                    ? GoogleMap(
                        initialCameraPosition: _cameraPosition,
                        myLocationButtonEnabled: true,
                        myLocationEnabled: true,
                        zoomGesturesEnabled: true,
                        zoomControlsEnabled: true,
                        onMapCreated: (GoogleMapController controller) {
                          _controller = (controller);
                          _controller.animateCamera(
                              CameraUpdate.newCameraPosition(_cameraPosition));
                        },
                        markers: this.myMarker(),
                        onTap: (latLng) {
                          if (mounted)
                            setState(() {
                              latlong = latLng;
                            });
                        })
                    : Container(),
              ]),
            ),
            TextField(
              cursorColor: colors.black,
              controller: locationController,
              readOnly: true,
              decoration: InputDecoration(
                icon: Container(
                  margin: EdgeInsetsDirectional.only(start: 20, top: 0),
                  width: 10,
                  height: 10,
                  child: Icon(
                    Icons.location_on,
                    color: Colors.green,
                  ),
                ),
                hintText: "pick up",
                border: InputBorder.none,
                contentPadding:
                    EdgeInsetsDirectional.only(start: 15.0, top: 12.0),
              ),
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: colors.grad1Color,
              ),
              child: Text("Update Location"),
              onPressed: () {
                if (widget.from == getTranslated(context, 'ADDADDRESS')) {
                  latitude = latlong.latitude.toString();
                  longitude = latlong.longitude.toString();
                } else if (widget.from ==
                    getTranslated(context, 'EDIT_PROFILE_LBL')) {
                  lat = latlong.latitude.toString();
                  long = latlong.longitude.toString();
                }
                callback(latlong.latitude, latlong.longitude);
                Navigator.pop(context);
              },
            ),
          ],
        )));
  }

  Set<Marker> myMarker() {
    if (_markers != null) {
      _markers.clear();
    }

    _markers.add(Marker(
      markerId: MarkerId(Random().nextInt(10000).toString()),
      position: LatLng(latlong.latitude, latlong.longitude),
    ));
    getLocation();

    return _markers;
  }

  Future<void> getLocation() async {
    List<Placemark> placemark =
        await placemarkFromCoordinates(latlong.latitude, latlong.longitude);

    var address;
    address = placemark[0].name;
    address = address + ", " + placemark[0].street;
    address = address + ", " + placemark[0].subLocality;
    address = address + ", " + placemark[0].locality;
    address = address + ", " + placemark[0].administrativeArea;
    address = address + ", " + placemark[0].country;
    locationController.text = address;
  }
}

class MiniMap extends StatelessWidget {
  MiniMap(
      {Key key,
      this.latitude,
      this.longitude,
      this.addressC,
      this.setAddressC,
      this.initialCameraPosition,
      this.onMapCreated})
      : super(key: key);

  final double latitude, longitude;
  final Function setAddressC;
  final Function onMapCreated;
  final TextEditingController addressC;
  final CameraPosition initialCameraPosition;
  final Set<Marker> markers = Set();

  @override
  Widget build(BuildContext context) {
    LatLng latLng = new LatLng(latitude, longitude);
    Set<Marker> marker = this.miniMapMarker(latLng);
    return Column(
      children: [
        Container(
          height: 170,
          child: GoogleMap(
            myLocationButtonEnabled: false,
            zoomControlsEnabled: false,
            initialCameraPosition: initialCameraPosition,
            markers: marker,
            onMapCreated: onMapCreated,
          ),
        ),
        TextField(
          cursorColor: colors.black,
          controller: addressC,
          readOnly: true,
          decoration: InputDecoration(
            hintText: "pick up",
            border: InputBorder.none,
          ),
        ),
      ],
    );
  }

  Set<Marker> miniMapMarker(LatLng latLng) {
    if (markers != null) {
      markers.clear();
    }

    markers.add(Marker(
      markerId: MarkerId(Random().nextInt(10000).toString()),
      position: latLng,
    ));

    return markers;
  }
}
