
removeFirstZeroPhoneNumber (String value) {
  String _modified; // modify var without first zero
  if(value[0] == '0'){
    _modified = value.substring(1, value.length);
  } else {
    _modified = value;
  }
  return _modified;
}
